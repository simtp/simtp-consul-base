FROM ubuntu:18.04

ARG CONSUL_VERSION

ENV CONSUL_CONFIG_FILE /etc/consul.d/default.hcl
ENV CONSUL_PARAMS -config-file=$CONSUL_CONFIG_FILE

EXPOSE 8500
EXPOSE 8600

RUN apt-get update; \
	apt-get upgrade; \
	apt-get install -y curl unzip; \
	apt-get clean 

RUN curl https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip > /consul_${CONSUL_VERSION}_linux_amd64.zip; \
	cd /; \
	unzip consul_${CONSUL_VERSION}_linux_amd64.zip;
RUN mv /consul /usr/bin
RUN rm -rf /consul_${CONSUL_VERSION}_linux_amd64.zip

RUN mkdir -p /var/consul; \
	mkdir -p /etc/consul.d/

COPY default.hcl /etc/consul.d/default.hcl

CMD consul agent $CONSUL_PARAMS


