datacenter		= "docker" 
data_dir		= "/var/consul"
log_level		= "INFO"
node_name		= "consul"
server			= true
ui			= true 
client_addr		= "{{ GetPrivateIP }}" 
bootstrap_expect	= 1 
connect { 
	enabled		= true 
}

